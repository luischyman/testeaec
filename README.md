# Teste AeC

## Backend
1. Fluent validator, foi criado todos os endpoints para mostrar que tenho a capacidade e muito além do que pediram
foi criado um service genérico para demonstrar a capacidade para mostrar a orientação de objeto tanto pelo service, quanto pelo repository
2. Esta sendo armazenado ao banco, após realizar a consulta dos dados do ativo.
3. Todo o backend foi preparado utilizando metodologias de clean code e entityFramework e arquitetura DDD
4. Banco de dados utilizado foi o SQL Server
5. Todo o sistema esta em .NET 7.0
6. Sistema foi criado com modelo de injeção de dependencia(Respeitando as camadas de interface e camadas de execução)

## Front-end
1. Selenium
2. NUnit, XUnit
3. WebDriver Chrome

## Como Rodar
1. É necessário o download do SQL Server, Visual Studio e o .Net 7.0 para a utilização do sistema
2. Clonar o projeto no gitlab
3. Rodar o sql da tabela de Ativos que está na pasta sql dentro da pasta do projeto
4. Rode o sistema
